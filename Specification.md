# Behametrics Specification 1.3.2

This document serves as a guideline for designing and implementing:
* software that logs (collects) data from devices such as a mouse or a mobile device, also referred to in this document as a *logger*,
* the format and contents of the logged data,
* software that processes the logged data for further analysis,
* software that transforms the logged data to a form more suitable for data analysis.

The specification focuses on data usable in the domain of behavioral biometrics
such as mouse movement and clicks or mobile device motion via accelerometer or gyroscope.

This specification is primarily based on the following API references:
* [Web APIs](https://developer.mozilla.org/en-US/docs/Web/API)
* [Android API](https://developer.android.com/reference/packages)


## Basic Concepts

A *logger* is a piece of software (usually a library or a standalone application)
responsible for collecting raw data from a device and storing the data locally or sending the data over the network.

A *device* represents a piece of hardware that allows users to interact with an application.
A device contains one or more *inputs*.
An input yields *input events*, also collectively referred to as *raw data*.
Each input event contains one or more *fields*.
Each field has a name (*field name*) and a value (*field value*).

The logged data can be represented (stored, sent) in various *formats* such as JSON or comma-separated values (CSV).

### Examples

A computer mouse is a device that allows controlling the cursor on a computer screen.
When a user performs a drag-and-drop operation, the first event the mouse yields is the pressing of the left mouse button,
followed by a series of mouse movement events, and finally ended by the release of the left mouse button.
For the first event, possible fields include the type of event (having the value `down`),
cursor coordinates on the screen (*x* and *y*) and the button pressed (`left`).
Movement events contain the same fields with different values for the event type (`move`) and the cursor coordinates.
The last event likewise contains the same fields with a different event type (`up`) and cursor coordinates.

Example of events obtained from a mouse formatted as a CSV file
showing the drag-and-drop operation:
```csv
input,session_id,timestamp,event_type,button,x,y
cursor,5c70579af2307d000b7eaf0c,1542730372913000000,down,left,410,1050
cursor,5c70579af2307d000b7eaf0c,1542730372923000000,move,left,412,1055
cursor,5c70579af2307d000b7eaf0c,1542730372935000000,move,left,414,1058
cursor,5c70579af2307d000b7eaf0c,1542730372946000000,move,left,417,1062
cursor,5c70579af2307d000b7eaf0c,1542730372953000000,up,left,419,1063
```

The input is named `cursor` as many other pointing devices beside the mouse are able to control the cursor
(such as a touchpad or a pointing stick).

A mobile device (phone, tablet, smart watch, etc.) contains multiple inputs such as touch screen, accelerometer or gyroscope.
The accelerometer periodically yields events describing the device acceleration on *x*, *y* and *z* axes separately.

Example of an accelerometer event formatted as JSON:
```json
{
    "input": "sensor_accelerometer",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "x": 0.4562,
        "y": 9.8041,
        "z": 4.4964
    }
}
```


## Formats

This section describes formats for logged input events that should be supported by a logger or a server with a database.

### JSON

Formatting input events as JSON objects is suitable for sending the events over the network and storing the events in a database.

Each JSON object contains a single input event in the following format:
```
{
    "input": <input name>,
    "session_id": <session ID>,
    "timestamp": <timestamp>,
    "content": {
      <input-specific fields...>
    }
}
```

`"content"` is an object holding fields specific to a particular input.

Example of an event:
```json
{
    "input": "sensor_accelerometer",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "x": 0.4562,
        "y": 9.8041,
        "z": 4.4964
    }
}
```

If an input event contains extra field values for which there are no corresponding field names,
the extra fields may be omitted, or may be included with arbitrary field names such as `value<number>`.
This may happen if, for example, an input for a particular hardware yields an array of values of a greater length than the logger expects.

Example of an accelerometer event with extra fields:
```json
{
    "input": "sensor_accelerometer",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "x": 0.4562,
        "y": 9.8041,
        "z": 4.4964,
        "value0": 0.453,
        "value1": 1
    }
}
```

None of the fields should contain nested objects or arrays (except the `"content"` field which is an object).
This avoids excessive complexity of the structure of an event
and simplifies transformation of JSON objects to other formats such as CSV.


### CSV

The comma-separated values (CSV) format is suitable for storing the data for the purpose of data analysis
(in local storage or on a server as a publicly available dataset).

Raw data from each input must be stored in separate CSV files.

A CSV filename must have the following format: `<input name>.csv`

The first line of a CSV file must be a header containing field names, at least those that are required for a particular input.

Field names in the CSV header must be separated by a comma (`,`).
Field values containing the separator character must be replaced with a different character or string.

If there are more field values in the file contents than field names in the header,
the missing field names may be omitted or filled with arbitrary names such as `value<number>`.

Example of a CSV file for cursor events named `cursor.csv`:
```csv
input,session_id,timestamp,event_type,button,x,y
cursor,5c70579af2307d000b7eaf0c,1542730372913000000,down,left,410,1050
cursor,5c70579af2307d000b7eaf0c,1542730372913000000,up,left,410,1050
cursor,5c70579af2307d000b7eaf0c,1542730372923000000,move,,412,1055
cursor,5c70579af2307d000b7eaf0c,1542730372935000000,move,,414,1058
cursor,5c70579af2307d000b7eaf0c,1542730372946000000,move,,417,1062
...
```

Example of a CSV file for cursor events named `cursor.csv` with extra field values:
```csv
input,session_id,timestamp,event_type,button,x,y
cursor,5c70579af2307d000b7eaf0c,1542730372913000000,down,left,410,1050,0.453,1
cursor,5c70579af2307d000b7eaf0c,1542730372913000000,up,left,410,1050,0.453,1
cursor,5c70579af2307d000b7eaf0c,1542730372923000000,move,,412,1055,0.453,1
cursor,5c70579af2307d000b7eaf0c,1542730372935000000,move,,414,1058,0.453,1
cursor,5c70579af2307d000b7eaf0c,1542730372946000000,move,,417,1062,0.453,1
...
```

Example of a CSV file for cursor events named `cursor.csv` with extra field values and arbitrary field names for the extra values:
```csv
input,session_id,timestamp,event_type,button,x,y,value0,value1
cursor,5c70579af2307d000b7eaf0c,1542730372913000000,down,left,410,1050,0.453,1
cursor,5c70579af2307d000b7eaf0c,1542730372913000000,up,left,410,1050,0.453,1
cursor,5c70579af2307d000b7eaf0c,1542730372923000000,move,,412,1055,0.453,1
cursor,5c70579af2307d000b7eaf0c,1542730372935000000,move,,414,1058,0.453,1
cursor,5c70579af2307d000b7eaf0c,1542730372946000000,move,,417,1062,0.453,1
...
```

#### Transforming JSON objects to CSV

This section provides guidelines to transform a group of JSON objects to a CSV file.

The header of a CSV file should consist of top-level field names of each JSON object, except the name `"content"`,
and top-level field names of each `"content"` object.

The columns should be ordered as follows:
* Known common fields in the order specified in this document.
* Unknown common fields, sorted alphabetically.
* Known input-specific fields in the order specified for a particular input.
* Unknown input-specific fields, sorted alphabetically.

Values of non-`"content"` fields that are nested objects or arrays should be treated as strings.


## Fields - General Guidelines

This section contains general guidelines for field names and their values.

### Field Names

Field names may only contain lowercase letters, numbers or an underscore (`_`).

A field name must not be empty.

Examples: `session_id`, `timestamp`, `event_type`

### Field Values

#### Enumerated Fields

Enumerated fields are fields containing a limited set of values.

Values for enumerated fields may only contain lowercase letters, numbers or an underscore (`_`).

For an enumerated field, a logger must support at least the values specified in each particular input
(see [Input Events](#input-events)).
A logger may provide additional enumerated values beside the required values.

Examples of enumerated values: `down`, `move`, `up`

#### Field Units

Unless stated otherwise for a particular field, this specification does not specify
the unit in which a field should be reported as measurement units may vary across different platforms or hardware.

### Field Order

The order of fields of input events in any format does not matter.
For improved readability, it is recommended to maintain the order as specified for the individual inputs
(and as shown in examples throughout the document),
if a particular format supports ordering (e.g. CSV does while JSON does not).


## Input Events

This section describes the contents of input events for each input.

For each input event, a description for each field is provided as well as an example in the JSON format.
Each entry describing an input-specific field has the following attributes:
* Field name.
* Brief description of the field, including constraints if any.
* Indication whether the field is required (i.e. must be included in every event for a particular input) or optional.
  Optional fields that a logger does not support should be completely omitted.
  For example, the logger should not include the `timestamp_precision` field in a CSV file
  (in the header or contents) if the logger does not support that field.
* Platforms the field should be supported on, if available.
  This specification currently supports the following platforms:
    * `Android` - applications on Android devices.
    * `Desktop` - desktop applications.
    * `Web` - websites or web applications.
    * `All` - all supported platforms.


### Common Fields for All Input Events

Each input event must support at least the following fields:

| Field name            | Description                                                                                                                                                 | Required? | Platforms |
|:----------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------|:----------|:----------|
| `input`               | Type of input, such as `cursor`, `touch` or `sensor_accelerometer`.                                                                                         | Yes       | All       |
| `session_id`          | Unique string identifying a single session.                                                                                                                 | Yes       | All       |
| `timestamp`           | Integer representing the time the event was acquired since the beginning of the epoch.                                                                      | Yes       | All       |
| `timestamp_precision` | Number of digits indicating `timestamp` precision, starting from seconds. If omitted, 9 is assumed, indicating that `timestamp` is reported in nanoseconds. | No        | All       |

The `session_id` may be used to distinguish e.g. multiple users or application sessions for the same user.
The `session_id` is also useful for linking multiple inputs such as touch, accelerometer and gyroscope from the same mobile device,
in order to perform e.g. multi-modal biometric user authentication.

Examples for `timestamp_precision`: value of 3 indicates that `timestamp` is reported in milliseconds,
6 indicates microseconds, 9 indicates nanoseconds, and so on.
Values other than multipliers of 3 are possible, e.g. 8 indicates 10 nanoseconds.

### Cursor

Input name: `cursor`

Any pointing device that is able to control the screen cursor is considered to have the cursor input.
Examples of pointing devices controlling the cursor include mouse, touchpad and pointing stick.

While it may seem sensible that the type of a pointing device should somehow be specified (e.g. as a field),
there is no reliable way on some platforms to determine what type of device is currently controlling the cursor (notably the web).

For events representing scrolling via the wheel button, there is a separate input named [`wheel`](#wheel).

##### Fields

| Field name   | Description                                                                              | Required? | Platforms    |
|:-------------|:-----------------------------------------------------------------------------------------|:----------|:-------------|
| `event_type` | Type of event.                                                                           | Yes       | Desktop, Web |
| `button`     | A button that is being pressed. If no button is being pressed, this field must be empty. | Yes       | Desktop, Web |
| `x`          | *x*-coordinate of the cursor on the screen.                                              | Yes       | Desktop, Web |
| `y`          | *y*-coordinate of the cursor on the screen.                                              | Yes       | Desktop, Web |

The following values for `event_type` must be supported:

| Value  | Description               |
|:-------|:--------------------------|
| `down` | The press of a button.    |
| `up`   | The release of a button.  |
| `move` | Pointing device movement. |

The following values for `button` must be supported:

| Value    | Description                          |
|:---------|:-------------------------------------|
| `left`   | The left button.                     |
| `right`  | The right button.                    |
| `middle` | The middle button or a wheel button. |

A *button* is considered to be a physical button (such as a mouse button) or a specific gesture simulating a button press
(such as a tap on a touchpad corresponding to a single click of the left mouse button).
Multiple buttons being simultaneously pressed (e.g. left and right button at once) should be represented as separate
input events.

##### Examples

```json
{
    "input": "cursor",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "event_type": "down",
        "button": "left",
        "x": 410,
        "y": 1050
    }
}
```

```json
{
    "input": "cursor",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "event_type": "move",
        "button": "",
        "x": 410,
        "y": 1050
    }
}
```

### Wheel

Input name: `wheel`

A wheel event occurs when a wheel button on a pointing device is rotated
or an action simulating wheel rotation is performed.

##### Fields

| Field name   | Description                                                                              | Required? | Platforms    |
|:-------------|:-----------------------------------------------------------------------------------------|:----------|:-------------|
| `button`     | A button that is being pressed. If no button is being pressed, this field must be empty. | Yes       | Desktop, Web |
| `delta_x`    | Scroll amount on the *x*-axis (horizontal scrolling).                                    | Yes       | Desktop, Web |
| `delta_y`    | Scroll amount on the *y*-axis (vertical scrolling).                                      | Yes       | Desktop, Web |
| `delta_z`    | Scroll amount on the *z*-axis.                                                           | No        | Desktop, Web |
| `delta_unit` | Unit in which the `delta_*` fields are specified.                                        | No        | Desktop, Web |
| `cursor_x`   | *x*-coordinate of the cursor on the screen.                                              | Yes       | Desktop, Web |
| `cursor_y`   | *y*-coordinate of the cursor on the screen.                                              | Yes       | Desktop, Web |

The `button` field has the same meaning and possible values as specified for the [cursor input](#cursor).

The following values for `delta_unit` should be supported:

| Value   | Description                                   |
|:--------|:----------------------------------------------|
| `pixel` | The `delta_*` fields are specified in pixels. |
| `line`  | The `delta_*` fields are specified in lines.  |
| `page`  | The `delta_*` fields are specified in pages.  |

##### Examples

```json
{
    "input": "wheel",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "button": "",
        "delta_x": 10,
        "delta_y": 0,
        "cursor_x": 410,
        "cursor_y": 1050
    }
}
```

### Keyboard

Input name: `keyboard`

Whenever a key on a keyboard (hardware or virtual) is pressed or released, a keystroke event is emitted.

##### Fields

| Field name   | Description                                                                       | Required? | Platforms |
|:-------------|:----------------------------------------------------------------------------------|:----------|:----------|
| `event_type` | Event type, such as a key press or release.                                       | Yes       | All       |
| `key_code`   | Code of the key pressed.                                                          | Yes       | All       |
| `key_name`   | A character or a modifier (e.g. `"shift"`) generated by pressing/releasing a key. | Yes       | All       |

The `key_code` field may be a number representing a Unicode character or a string uniquely identifying the physical key pressed.
This allows, for instance, distinguishing a number pressed via a typewriter key (`4`) and the same number pressed on the numeric keypad (`Numpad4`), or the left and the right shift key.
If Unicode numbers are used to represent key codes, then modifier keys may be represented by an arbitrary number or string since they have no mapping to any Unicode character
(but should, naturally, be used consistently for all presses and releases of that key).

NOTE: Logging individual keystrokes may pose a major threat to privacy (compared to other inputs in this specification)
and should therefore be limited to research purposes.

The following values for `event_type` must be supported:

| Value  | Description              |
|:-------|:-------------------------|
| `down` | Indicates a key press.   |
| `up`   | Indicates a key release. |

##### Examples

```json
{
    "input": "keyboard",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "event_type": "down",
        "key_code": "101",
        "key_name": "e"
    }
}
```

```json
{
    "input": "keyboard",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "event_type": "up",
        "key_code": "Numpad4",
        "key_name": "4"
    }
}
```

### Touch Screen

Input name: `touch`

##### Fields

| Field name          | Description                                                                                                           | Required? | Platforms    |
|:--------------------|:----------------------------------------------------------------------------------------------------------------------|:----------|:-------------|
| `event_type`        | Type of touch event.                                                                                                  | Yes       | Android, Web |
| `event_type_detail` | A more detailed description for `event_type`. Should be equivalent to `event_type` if no further detail is specified. | Yes       | Android, Web |
| `pointer_id`        | An integer identifying the touch pointer (finger) currently pressed on the touch screen.                              | Yes       | Android, Web |
| `x`                 | *x*-coordinate of a touch pointer on the touch screen, relative to the active window.                                 | Yes       | Android, Web |
| `y`                 | *y*-coordinate of a touch pointer on the touch screen, relative to the active window.                                 | Yes       | Android, Web |
| `pressure`          | Pressure applied on the touch screen for a touch pointer.                                                             | No        | Android, Web |
| `size`              | Size of a touch pointer pressing the touch screen.                                                                    | No        | Android      |
| `touch_major`       | Length of the major axis of an ellipse approximating the area of a touch pointer pressing the touch screen.           | No        | Android      |
| `touch_minor`       | Length of the minor axis of an ellipse approximating the area of a touch pointer pressing the touch screen.           | No        | Android      |
| `raw_x`             | *x*-coordinate of a touch pointer on the touch screen, relative to the entire touch screen.                           | No        | Android      |
| `raw_y`             | *y*-coordinate of a touch pointer on the touch screen, relative to the entire touch screen.                           | No        | Android      |

The following values for `event_type` must be supported:

| Value    | Description                                                                                                       |
|:---------|:------------------------------------------------------------------------------------------------------------------|
| `down`   | The press of a touch pointer on the touch screen.                                                                 |
| `up`     | The release of a touch pointer from the touch screen.                                                             |
| `move`   | Touch movement.                                                                                                   |
| `cancel` | Event indicating a touch movement was cancelled. The occurrence of touch cancellation is implementation-specific. |
| `other`  | A misc. touch event (such as cancellation) whose detail should be specified in `event_type_detail`.               |

Multi-touch gestures (that involve performing actions with multiple touch pointers at once) should be represented as separate input events.

The `pressure` and `size` fields effectively refer to the same measurement.
Depending on the Android device, usually either `pressure` or `size` yields relevant values.
A logger for Android devices should log both fields.
A person responsible for data analysis should then determine which field is to be used for further processing, discarding the other.

##### Examples

```json
{
    "input": "touch",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "event_type": "move",
        "event_type_detail": "move",
        "pointer_id": 0,
        "x": 456.124,
        "y": 712.083,
        "raw_x": 456.124,
        "raw_y": 712.083
    }
}
```

```json
{
    "input": "touch",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730373014000000,
    "content": {
        "event_type": "down",
        "event_type_detail": "pointer_down",
        "pointer_id": 1,
        "x": 625.097,
        "y": 413.561,
        "raw_x": 625.097,
        "raw_y": 413.561
    }
}
```

```json
{
    "input": "touch",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730373014000000,
    "content": {
        "event_type": "other",
        "event_type_detail": "outside",
        "pointer_id": 1,
        "x": 625.097,
        "y": 413.561,
        "raw_x": 625.097,
        "raw_y": 413.561
    }
}
```

### Accelerometer

Input name: `sensor_accelerometer`

##### Fields

| Field name | Description                          | Required? | Platforms    |
|:-----------|:-------------------------------------|:----------|:-------------|
| `x`        | Device acceleration on the *x*-axis. | Yes       | Android, Web |
| `y`        | Device acceleration on the *y*-axis. | Yes       | Android, Web |
| `z`        | Device acceleration on the *z*-axis. | Yes       | Android, Web |

##### Examples

```json
{
    "input": "sensor_accelerometer",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "x": 0.4562,
        "y": 9.8041,
        "z": 4.4964
    }
}
```


### Linear Acceleration

Input name: `sensor_linear_acceleration`

##### Fields

| Field name | Description                                                              | Required? | Platforms    |
|:-----------|:-------------------------------------------------------------------------|:----------|:-------------|
| `x`        | Device acceleration on the *x*-axis without the contribution of gravity. | Yes       | Android, Web |
| `y`        | Device acceleration on the *y*-axis without the contribution of gravity. | Yes       | Android, Web |
| `z`        | Device acceleration on the *z*-axis without the contribution of gravity. | Yes       | Android, Web |

##### Examples

```json
{
    "input": "sensor_linear_acceleration",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "x": 0.4562,
        "y": 0.0541,
        "z": 4.4964
    }
}
```

### Gyroscope

Input name: `sensor_gyroscope`

##### Fields

| Field name | Description                              | Required? | Platforms    |
|:-----------|:-----------------------------------------|:----------|:-------------|
| `x`        | Rate of device rotation on the *x*-axis. | Yes       | Android, Web |
| `y`        | Rate of device rotation on the *y*-axis. | Yes       | Android, Web |
| `z`        | Rate of device rotation on the *z*-axis. | Yes       | Android, Web |

##### Examples

```json
{
    "input": "sensor_gyroscope",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "x": 7.5451,
        "y": -15.0437,
        "z": 0.1218
    }
}
```

### Magnetic Field

Input name: `sensor_magnetic_field`

Also referred to as *magnetometer*.

##### Fields

| Field name | Description                                  | Required? | Platforms    |
|:-----------|:---------------------------------------------|:----------|:-------------|
| `x`        | Magnetic field around the device's *x*-axis. | Yes       | Android, Web |
| `y`        | Magnetic field around the device's *y*-axis. | Yes       | Android, Web |
| `z`        | Magnetic field around the device's *z*-axis. | Yes       | Android, Web |

##### Examples

```json
{
    "input": "sensor_magnetic_field",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "x": -16.792,
        "y": -21.679,
        "z": -23.501
    }
}
```

### Metadata

Input name: `metadata`

Metadata serve to identify or describe a device in further detail, such as platform (web, Android, etc.).
To associate multiple sessions with the same device, make sure you log metadata every time the session ID is changed in your application.

##### Fields

| Field name        | Description       | Required? | Platforms |
|:------------------|:------------------|:----------|:----------|
| `logger_platform` | Type of platform. | Yes       | All       |

Beside the required fields, metadata may contain additional arbitrary fields.

The `logger_platform` field should contain one of the following values,
or a different value if none of the following values represent the platform:

| Value     | Description                      |
|:----------|:---------------------------------|
| `android` | Applications on Android devices. |
| `desktop` | Desktop applications.            |
| `web`     | Websites or web applications.    |

##### Examples

```json
{
    "input": "metadata",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "logger_platform": "android",
        "version": "8.1"
    }
}
```

### Custom Events

Input name: `custom`

Beside the [common fields](#common-fields-for-all-input-events), the contents of a custom event are completely arbitrary,
as the input name suggests.
In other words, there are no required or optional fields defined for this input.

##### Examples

```json
{
    "input": "custom",
    "session_id": "5c70579af2307d000b7eaf0c",
    "timestamp": 1542730372913000000,
    "content": {
        "action": "login",
        "username": "MyUsername"
    }
}
```


## Additional Requirements for Loggers

The `timestamp` field must be reported with a precision of at least 10 milliseconds (in order to obtain usable data).

A logger may support any subset of inputs specified in this document.

A logger may allow defining additional inputs by application developers beside the inputs specified in this document.

If HTTP(S) requests are used to send input events, each request should contain a header entry named `"Logger"`.
The value for `"Logger"` has no strict format, but should contain information to identify the platform (web, Android, etc.) and the logger version.
