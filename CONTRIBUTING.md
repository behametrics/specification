# Contributing to Behametrics Specification

Thank you for taking your time to contribute to Behametrics Specification.
To help improve the project, we would like you to follow guidelines specified in this document.

## Submitting Changes

1. Make sure your proposed change is not already implemented (by searching the documentation, closed issues or merge requests).
2. Fork the repository.
3. Create a new, appropriately named branch:

       git checkout -b add-accelerometer-sensor master

4. Commit your changes to the new branch.
5. Push your changes to the remote repository:

       git push origin add-accelerometer-sensor

6. Create a new merge request on GitLab with `master` as the target branch.
