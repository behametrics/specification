# Behametrics Specification

This is a specification documenting the requirements for
software that logs (collects) data from devices,
the format and contents of the logged data,
and software that processes the logged data.

## [Latest Official Release](https://gitlab.com/behametrics/specification/blob/release/Specification.md)

## [Latest Work-in-Progress Version](https://gitlab.com/behametrics/specification/blob/master/Specification.md)

## Contributing to Specification

See [CONTRIBUTING](CONTRIBUTING.md).

## License

The specification is provided under the [MIT License](LICENSE).
